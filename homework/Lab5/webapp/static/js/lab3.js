$(document).ready(function() {
    // establish global variables for LED status
    var led1;
    var led2;
    var led3;
    // establish global variables for temperature
    var cached_sensor_readings = [];
    var temp_data = [];
    var tempGraph = new Morris.Line({
        element: 'mytempchart',
        data: [],
        xkey: 'time',
        ykeys: ['temp'],
        labels: ['Temperature (C)']
    });
    // establish global variable for pressure
    var cached_pressure_readings = [];
    var press_data = [];
    var pressGraph = new Morris.Line({
        element: 'mypresschart',
        data: [],
        xkey: 'time',
        ykeys: ['press'],
        labels: ['Pressure (hPa)']
    });
    
    // update the Switch based on its SSE state monitor
    function updateSwitch(switchValue) {
       if (switchValue === '1') {
          $('#switch').text('ON');
          $('#switch').css('background', 'red');
          console.log('HERE');
       } else if (switchValue === '0') {
          $('#switch').text('OFF');
          $('#switch').css('background', 'grey');
       }
    }
    
    // update the LEDs based on their SSE state monitor
    function updateLeds(ledNum, ledValue) {
			led_label = '';
			led_text = 'Unknown';
			led_btn_clr = 'white';
			led_btn_label = '';
			if (ledNum === 1) {
					led_label = '#red_led_label';
					led_btn_clr = 'red';
					led_btn_label = '#red_led_btn';
			} else if (ledNum === 2) {
					led_label = '#grn_led_label';
					led_btn_clr = 'green';
					led_btn_label = '#grn_led_btn';
			} else if (ledNum === 3) {
					led_label = '#yel_led_label';
					led_btn_clr = 'yellow';
					led_btn_label = '#yel_led_btn';
			}
			if (ledValue === '1') {
					led_text = "ON";
					$(led_label).css('color', led_btn_clr);
					//$(led_btn_label).css('background', led_btn_clr);
			} else if (ledValue === '0') {
					led_text = "OFF";
					$(led_label).css('color', 'white');
					//$(led_btn_label).css('background', 'white');
			} 
			// led_label.text(led_text);

    }      
    
    // Let's read the current LED state
    function initial_conditions() {
       var d = $.Deferred();
    
       setTimeout(function() {
          $.get('/leds/1',function(data){
             led1 = $.trim(data.split(':')[1]);
          });
    
          $.get('/leds/2',function(data){
             led2 = $.trim(data.split(':')[1]);
          });
    
          $.get('/leds/3',function(data){
             led3 = $.trim(data.split(':')[1]);
          });
    
          console.log("Got my data now!");
          d.resolve();
       }, 10);
       return d.done();
    }
    
    // Let's initialize our LED vars to the current LED state "ON"/"OFF"
    function led_status() {
       var d = $.Deferred();
    
       setTimeout(function() {
          if (led1 === '0') {led1 =  "OFF"} else {led1 =  "ON"}
          if (led2 === '0') {led2 =  "OFF"} else {led2 =  "ON"}
          if (led3 === '0') {led3 =  "OFF"} else {led3 =  "ON"}
          d.resolve();
    
          console.log("RED:",led1);
          console.log("GRN:",led2);
          console.log("YEL:",led3);
        }, 100);
        return d.promise();
    }
    
    // intercept the incoming states from SSE
    iotSource.onmessage = function(e) {
        console.log(e.data);
        sse_data = JSON.parse(e.data);
        console.log(sse_data);

        updateSwitch(sse_data["sw"]);
        updateLeds(1, sse_data["led1"]);
        updateLeds(2, sse_data["led2"]);
        updateLeds(3, sse_data["led3"]);

        cached_sensor_readings.unshift({
            'time': Date.parse(sse_data['meas_time']),
            'temp': sse_data['temperature']['reading'],
            'press': sse_data['pressure']['reading']
        });
        while (cached_sensor_readings.length > 20) cached_sensor_readings.pop();
        //console.log(cached_sensor_readings.length);

        updateDataTable();
        updateTempChart();
        updatePressChart();
    }  
    
    
    function updateTempChart() {
        tempGraph.setData(cached_sensor_readings);
    }

    function updateDataTable() {
        rStr = "";
        cached_sensor_readings.slice(0, 5).forEach(function(di) {
            rStr += "<tr>";
            rStr += "<td>" + di['time'] + "</td>";
            rStr += "<td>" + di['temp'] + "</td>";
            rStr += "<td>" + di['press'].toFixed(4) + "</td>";
            rStr += "</tr>";
            //console.log(di);
        });
        //console.log("updateSensorData(): -- " + rStr);
        $("tbody#sensor-data").html(rStr);
    }
    
    function updatePressChart() {
        pressGraph.setData(cached_pressure_readings);
    }
    
    // make sure to intialize synchronously (10ms back to back)
    initial_conditions().then(led_status);
    
    // The button click functions run asynchronously in the browser
    $('#red_led_btn').click(function() {
       if (led1 === "OFF") {
          led1 = "ON";
       } else {
          led1 = "OFF";
       }
       console.log('red button click');
       var params = 'led=1&state=' + led1;
       console.log('Led Command with params:' + params);
       $.post('/ledcmd', params, function(data, status) {
          console.log("Data: " + data + "\nStatus: " + status);
       });
    });
    
    // The button click functions run asynchronously in the browser
    $('#grn_led_btn').click(function() {
       if (led2 === "OFF") {
           led2 = "ON";
       } else {
       led2 = "OFF";
       }
       var params = 'led=2&state=' + led2;
       console.log('Led Command with params:' + params);
       $.post('/ledcmd', params, function(data, status) {
          console.log("Data: " + data + "\nStatus: " + status);
       });
    });
    
    // The button click functions run asynchronously in the browser
    $('#yel_led_btn').click(function() {
       if (led3 === "OFF") {
           led3 = "ON";
       } else {
         led3 = "OFF";
       }
       var params = 'led=3&state=' + led3;
       console.log('Led Command with params:' + params);
       $.post('/ledcmd', params, function(data, status) {
          console.log("Data: " + data + "\nStatus: " + status);
       });
     }); 
    
     
     
             
}); 
