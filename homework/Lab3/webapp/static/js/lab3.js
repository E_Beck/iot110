$(document).ready(function() {
    // establish global variables for LED status
    var led1;
    var led2;
    var led3;
    
    // var switchSource = new EventSource("{{ url_for('myData')  }}");
    

    // update the Switch based on its SSE state monitor
    function updateSwitch(switchValue) {
       if (switchValue === '1') {
          $('#switch').text('ON');
          $('#switch').css('background', 'red');
          console.log('HERE');
       } else if (switchValue === '0') {
          $('#switch').text('OFF');
          $('#switch').css('background', 'grey');
       }
    }
    
    // update the LEDs based on their SSE state monitor
    function updateLeds(ledNum, ledValue) {
			led_label = '';
			led_text = 'Unknown';
			led_btn_clr = 'white';
			led_btn_label = '';
			if (ledNum === 1) {
					led_label = '#red_led_label';
					led_btn_clr = 'red';
					led_btn_label = '#red_led_btn';
			} else if (ledNum === 2) {
					led_label = '#grn_led_label';
					led_btn_clr = 'green';
					led_btn_label = '#grn_led_btn';
			} else if (ledNum === 3) {
					led_label = '#yel_led_label';
					led_btn_clr = 'yellow';
					led_btn_label = '#yel_led_btn';
			}
			if (ledValue === '1') {
					led_text = "ON";
					$(led_label).css('color', led_btn_clr);
					//$(led_btn_label).css('background', led_btn_clr);
			} else if (ledValue === '0') {
					led_text = "OFF";
					$(led_label).css('color', 'white');
					//$(led_btn_label).css('background', 'white');
			} 
			// led_label.text(led_text);

    }      
    
    // Let's read the current LED state
    function initial_conditions() {
       var d = $.Deferred();
    
       setTimeout(function() {
          $.get('/leds/1',function(data){
             led1 = $.trim(data.split(':')[1]);
          });
    
          $.get('/leds/2',function(data){
             led2 = $.trim(data.split(':')[1]);
          });
    
          $.get('/leds/3',function(data){
             led3 = $.trim(data.split(':')[1]);
          });
    
          console.log("Got my data now!");
          d.resolve();
       }, 10);
       return d.done();
    }
    
    // Let's initialize our LED vars to the current LED state "ON"/"OFF"
    function led_status() {
       var d = $.Deferred();
    
       setTimeout(function() {
          if (led1 === '0') {led1 =  "OFF"} else {led1 =  "ON"}
          if (led2 === '0') {led2 =  "OFF"} else {led2 =  "ON"}
          if (led3 === '0') {led3 =  "OFF"} else {led3 =  "ON"}
          d.resolve();
    
          console.log("RED:",led1);
          console.log("GRN:",led2);
          console.log("BLU:",led3);
        }, 100);
        return d.promise();
    }
    
    // intercept the incoming states from SSE
    switchSource.onmessage = function(e) {
       // console.log(e.data);
       
       params = e.data.split(' ');
       updateSwitch(params[0]);
       updateLeds(1, params[1]);
       updateLeds(2, params[2]);
       updateLeds(3, params[3]);
       console.log("!*!*!**!*!into switch function" + params[0]);
    }  
    
    
    // make sure to intialize synchronously (10ms back to back)
    initial_conditions().then(led_status);
    
    // The button click functions run asynchronously in the browser
    $('#red_led_btn').click(function() {
       if (led1 === "OFF") {
          led1 = "ON";
       } else {
          led1 = "OFF";
       }
       console.log('red button click');
       var params = 'led=1&state=' + led1;
       console.log('Led Command with params:' + params);
       $.post('/ledcmd', params, function(data, status) {
          console.log("Data: " + data + "\nStatus: " + status);
       });
    });
    
    // The button click functions run asynchronously in the browser
    $('#grn_led_btn').click(function() {
       if (led2 === "OFF") {
           led2 = "ON";
       } else {
       led2 = "OFF";
       }
       var params = 'led=2&state=' + led2;
       console.log('Led Command with params:' + params);
       $.post('/ledcmd', params, function(data, status) {
          console.log("Data: " + data + "\nStatus: " + status);
       });
    });
    
    // The button click functions run asynchronously in the browser
    $('#yel_led_btn').click(function() {
       if (led3 === "OFF") {
           led3 = "ON";
       } else {
         led3 = "OFF";
       }
       var params = 'led=3&state=' + led3;
       console.log('Led Command with params:' + params);
       $.post('/ledcmd', params, function(data, status) {
          console.log("Data: " + data + "\nStatus: " + status);
       });
     });         
}); 
