# time library used to continuously run thread to provide updates on LED status
import time
from gpio import PiGPio
from flask import *
from debouncer import Debouncer

app = Flask(__name__)
pi_gpio = PiGPio()
db = Debouncer()

# default route so status can be returned
@app.route("/")
def index():
    return render_template('index_bs.html')
    # index_eb.html tested SAT
    # use index_bs.html to test lab3.js file...
    # create an instance of my pi gpio object class.
    #pi_gpio = PiGPio()
    #switch_state = pi_gpio.read_switch()
    #led1_state = pi_gpio.get_led(1)
    #led2_state = pi_gpio.get_led(2)
    #led3_state = pi_gpio.get_led(3)
    #print led3_state
    #return render_template('index_bs.html', switch=switch_state,
    #                             led1=led1_state,
    #                             led2=led2_state,
    #                             led3=led3_state)    

# ============================== API Routes ===================================
# ============================ GET: /leds/<state> =============================
# read the LED status by GET method from curl for example
# curl http://speedwagon:5000/leds/1
# curl http://speedwagon:5000/leds/2
# curl http://speedwagon:5000/leds/3
# -----------------------------------------------------------------------------
@app.route("/leds/<int:led_state>", methods=['GET'])
def leds(led_state):
	return "LED State:" + str(pi_gpio.get_led(led_state)) + "\n"


# =============================== GET: /sw ====================================
# read the switch input by GET method from curl for example
# curl http://iot8e3c:5000/sw
# -----------------------------------------------------------------------------
@app.route("/sw", methods=['GET'])
def sw():
  	return "Switch State:" + str(pi_gpio.read_switch()) + "!\n"

# ======================= POST: /ledcmd/<data> =========================
# set the LED state by POST method from curl. For example:
# curl --data 'led=1&state=ON' http://iot8e3c:5000/ledcmd
# -----------------------------------------------------------------------------
@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    cmd_data = request.data
    print "LED Command:" + cmd_data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pi_gpio.set_led(led,False)
    elif (state == 'ON'):
        pi_gpio.set_led(led,True)
    else:
        return "Argument Error"

    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"
# ======================== Endpoint: /mydata ==================================
#
#  Add the routing endoint code to read the switch status as well as LEDs.  This
#  endpoint will read the status of the switch, 3 LEDs and then build formatted 
#  data
#
#  read the gpio states by GET method from curl for example
#  curl http://pi.../myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
	#pi_gpio = PiGPio()
    def get_state_values():
	    while True:
	        # return the yield results on each loop, INFINITE LOOP
	    	raw_switch = pi_gpio.read_switch()
	    	debounced_switch = str(db.debounce(raw_switch))
	    	led_red = str(pi_gpio.get_led(1))
	    	led_grn = str(pi_gpio.get_led(2))
	    	led_yel = str(pi_gpio.get_led(3))
	    	# yield returns without giving up control
	    	yield('data: {0} {1} {2} {3}\n\n'.format(debounced_switch, led_red, led_grn, led_yel))
	    	time.sleep(0.05)
	# returning a function, that's a server sent event (this is going out to the client (index.html)    	
    return Response(get_state_values(), mimetype='text/event-stream')
# ============================== API Routes ===================================                                

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
