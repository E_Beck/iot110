#import the GPIO and time package
from gpio import PiGPio
import time

pigpio = PiGPio()
try:
	while True:
    	    switch = pigpio.read_switch()
	    print('\n ====== Switch: {0} ======'.format(switch))
	    print('\nLED 1 ON (RED)')
	    pigpio.set_led(1, True)
	    # pigpio.set_led(2, True)
	    print('\n LED1: {0}'.format(pigpio.get_led(1)))
	    time.sleep(1.0)
          
	    print('\nLED 1 OFF (RED)')
	    pigpio.set_led(1, False)
	    # pigpio.set_led(2, True)
	    print('\n LED1: {0}'.format(pigpio.get_led(1)))
	    time.sleep(1.0)
    
except KeyboardInterrupt:
         pigpio.set_led(1, False)
         pigpio.set_led(2, False)
         pigpio.set_led(3, False)
         print('\n^C interrupted')
         # GPIO.cleanup() not defined in class created