$(document).ready(function() {
var led1;
var led2;
var led3;

var max_data_saved = 4;
//Add variable to hold data between receive data events
var sse_sensor_data = [];

// var iotSource = new EventSource("{{ url_for('myData') }}");

iotSource.onmessage = function(e) {
  // must convert all single quoted data with double quote format
  var dqf = e.data.replace(/'/g, '"');
  // now we can parse into JSON
  d = JSON.parse(dqf);
  // updateSwitch(d["switch"]);
  updateLeds(1, d["led_red"]);
  updateLeds(2, d["led_grn"]);
  updateLeds(3, d["led_yel"]);
  updateSensors(d);
  
  // Push new data to front of saved data list
  sse_sensor_data.unshift(d);

  // Then remove oldest data up to maximum data saved
  while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

  
  //console.log(d);
}



// update the LEDs based on their SSE state monitor
function updateLeds(ledNum,ledValue) {
  if (ledNum === 1) {
    if (ledValue === '1') {
      $('#red_led_label').toggleClass( 'label-default', false);
      $('#red_led_label').toggleClass( 'label-danger', true);
      led1 = "ON"
    } else if (ledValue === '0') {
      $('#red_led_label').toggleClass( 'label-default', true);
      $('#red_led_label').toggleClass( 'label-danger', false);
      led1 = "OFF"
    }
  }
  else if (ledNum === 2) {
    if (ledValue === '1') {
      $('#grn_led_label').toggleClass( 'label-default', false);
      $('#grn_led_label').toggleClass( 'label-success', true);
      led2 = "ON"
    } else if (ledValue === '0') {
      $('#grn_led_label').toggleClass( 'label-default', true);
      $('#grn_led_label').toggleClass( 'label-success', false);
      led2 = "OFF"
    }
  }
  else if (ledNum === 3) {
    if (ledValue === '1') {
      $('#yel_led_label').toggleClass( 'label-default', false);
      $('#yel_led_label').toggleClass( 'label-primary', true);
      led3 = "ON"
    } else if (ledValue === '0') {
      $('#yel_led_label').toggleClass( 'label-default', true);
      $('#yel_led_label').toggleClass( 'label-primary', false);
      led3 = "OFF"
    }
  }
}

// The button click functions run asynchronously in the browser
$('#red_led_btn').click(function() {
  if (led1 === "OFF") {led1 = "ON";} else {led1 = "OFF";}
  var params = 'led=1&state='+led1;
  console.log('Led Command with params:' + params);
  $.post('/ledcmd', params, function(data, status){
          console.log("Data: " + data + "\nStatus: " + status);
  });
});

// The button click functions run asynchronously in the browser
$('#grn_led_btn').click(function() {
  if (led2 === "OFF") {led2 = "ON";} else {led2 = "OFF";}
  var params = 'led=2&state='+led2;
  console.log('Led Command with params:' + params);
  $.post('/ledcmd', params, function(data, status){
          console.log("Data: " + data + "\nStatus: " + status);
  });
});
// The button click functions run asynchronously in the browser
$('#yel_led_btn').click(function() {
  if (led3 === "OFF") {led3 = "ON";} else {led3 = "OFF";}
  var params = 'led=3&state='+led3;
  console.log('Led Command with params:' + params);
  $.post('/ledcmd', params, function(data, status){
          console.log("Data: " + data + "\nStatus: " + status);
  });
});



MBAR_TO_inHG = 0.029529983071;
var data = [];

// from http://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript
function zeropad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
}

function getDateNow() {
  var d = new Date();
  var date = (d.getFullYear()) + '-' + d.getMonth() + 1 + '-' + d.getDate();
  var time = zeropad(d.getHours(),2) + ':' + zeropad(d.getMinutes(),2) + ':' + zeropad(d.getSeconds(),2);
  return {epoch: time, date: (date + " " + time)};
}

updateSensors = (function (d) {
  var t_c = d['temperature'].reading
  var p_mbar = d['pressure'].reading
  var t_f = (t_c * 9) / 5.0 + 32.0;
  var p_inHg = p_mbar * MBAR_TO_inHG;

  var timedata = getDateNow();
  var t =  t_c.toFixed(1) + ' | ' + t_f.toFixed(1);
  var p =  p_mbar.toFixed(1) + ' | ' + p_inHg.toFixed(2);

  var obj = {};
  obj['date'] = timedata.date;
  obj['time'] = timedata.date;
  obj['temp'] = t;
  obj['press'] = p;
  data.push(obj);

  //console.log(timedata);
  if (data.length > 5) {
    data.shift();
    clearTable();
    updateTable(data);
    //update_temp_chart(data);
    //update_press_graph(data);
  }
});



function updateTable(data) {
  $('tr.param-row').each(function(i) {
    var tm = '<td>' + data[i]['date'] + '</td>';
    var temp = '<td>' + data[i]['temp'] + '</td>';
    var press = '<td>' + data[i]['press'] + '</td>';
    $(this).append(tm);
    $(this).append(temp);
    $(this).append(press);
  });
}

function clearTable() {
  $('tr.param-row').each(function(i) {
    $(this).empty();
  });
}

// ============================== SLIDER TABLE =================================

// LED 1 SLIDER
$("#slider1").slider({
    orientation: "vertical",
    min: 0,
    max: 100,
    value: 50,
    animate: true,
    slide: function(event, ui) {
        var dcValue = ui.value;
        $("#pwm1").val(dcValue);
        console.log("red led duty cycle(%):", ui.value);
        $.post('/set_pwm', { dutyCycle: ui.value });
    }
});
$("#pwm1").val($("#slider1").slider("value"));

// ===================================================================
// LED 2 SLIDER
$( "#slider2" ).slider({
    orientation: "vertical",
    range: "min",
    min: 0,
    max: 100,
    value: 50,
    animate: true,
    slide: function( event, ui ) {
      $( "#pwm2" ).val( ui.value );
      console.log("grn led duty cycle(%):",ui.value);
      $.post('/set_pwm', { dutyCycle: ui.value });
    }
});
$("#pwm2").val($("#slider2").slider("value"));
// ===================================================================

// ============================ STEPPER MOTOR ===============================
// Buttons
$('#motor_start').click(function() {
console.log('Start Motor Up!');
$.get('/motor/1');
});
$('#motor_stop').click(function() {
console.log('Stop Motor');
$.get('/motor/0');
});
$('#motor_zero').click(function() {
console.log('Zero Motor Position');
$.get('/motor_zero');
});
$('#motor_multistep').click(function() {
var params = 'steps='+$('#motor_steps').val()+"&direction="+$('#motor_direction').val();
console.log('Multistep with params:' + params);
$.post('/motor_multistep', params, function(data, status){
            console.log("Data: " + data + "\nStatus: " + status);
        });
});

// Text Fields
$('#motor_speed').change(function() {
console.log('Changed motor speed to ' + $('#motor_speed').val());
$.get('/motor_speed/'+$('#motor_speed').val());
});
$('#motor_position').change(function() {
console.log('Changed motor position to ' + $('#motor_position').val());
$.get('/motor_position/'+$('#motor_position').val());
});

$('#motor_steps').change(function() {
console.log('Changed motor steps to ' + $('#motor_steps').val());
$.get('/motor_steps/'+$('#motor_steps').val());
});

$('#motor_direction').change(function() {
console.log('Changed motor steps to ' + $('#motor_direction').val());
$.get('/motor_direction/'+$('#motor_direction').val());
});

// ============================ STEPPER MOTOR ===============================
function updateStepperMotor(data) {
$('#motor_position').text(data['motor']['position']);
if (data['motor']['state'] === '1') {
  $('#motor_state').toggleClass('label-default', false);
  $('#motor_state').toggleClass('label-success', true);
} else if (data['motor']['state'] === '0') {
  $('#motor_state').toggleClass('label-default', true);
  $('#motor_state').toggleClass('label-success', false);
}
}
// ============================ STEPPER MOTOR ===============================



});
