import RPi.GPIO as GPIO

# object oriented Python module to handle I/O for project

LED1_PIN = 18 #extension board pin RED 29 (RED for Lab7)
LED2_PIN = 13  #extension board pin GREEN 6 (GREEN for Lab7)
LED3_PIN = 23 #extension board pin YELLOW 23 (YELLOW for Lab7)
SWITCH_PIN =  26 #extension board pin push button 25

# declare the name of hte class that will instantiate GPIO object from web server code

class PiGPio(object):
	"""Raspberry Pi Internet 'IoT GPIO'."""

# define what happends when initialize driver in an init method

        def __init__(self):
		GPIO.setwarnings(False)
		#GPIO.setmode(GPIO.BOARD)
	     	GPIO.setmode(GPIO.BCM)
		GPIO.setup(LED1_PIN, GPIO.OUT)
	     	GPIO.setup(LED2_PIN, GPIO.OUT)
	     	GPIO.setup(LED3_PIN, GPIO.OUT)
	     	GPIO.setup(SWITCH_PIN, GPIO.IN)
	     	GPIO.setup(SWITCH_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# define method of reading the input switch pin.  NOTE: logic
# is inverted due to momentary switch circuit that grounds out 
# the input upon pressing the switch down

        def read_switch(self):
	     	"""Read the switch state."""
	     	switch = GPIO.input(SWITCH_PIN)
	     	 # invert because of active low momentary switch
	     	if (switch == 0):
			switch=1
	     	else:
			switch=0
		return switch

# create getter and setter methods for LED control
# set LED to ON or OFF
        def set_led(self, led, value):
	      	"""Change LED to the passed value, On (1) or off (0)."""
	      	if(led ==1):
			GPIO.output(LED1_PIN, value)
	      	if(led ==2):
			GPIO.output(LED2_PIN, value)
	      	if(led ==3):
			GPIO.output(LED3_PIN, value)

        # get the state of an LED
        def get_led(self, led):
	      	"""Return the state of the LED, On (1) or off (0)."""
	      	if(led ==1):
			return GPIO.input(LED1_PIN)
	      	if(led ==2):
			return GPIO.input(LED2_PIN)
	      	if(led ==3):
			return GPIO.input(LED3_PIN)

