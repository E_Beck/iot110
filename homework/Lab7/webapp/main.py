#!/usr/bin/python
import time
import datetime
import pprint
from stepper import PiStepper
from pwm import PiPwm
from gpio import PiGPio
from bmp280 import PiBMP280
# from debouncer import Debouncer
from flask import *


# create Objects
pi_gpio = PiGPio()
pi_smc = PiStepper()
pi_pwm = PiPwm()
pi_pwm_channel = None

# create an array of my pi bmp280 sensor dictionaries
sensor = {"name" : "bmp280", "addr" : 0x76, "chip" : PiBMP280(0x76) , "data" : {}}
(chip_id, chip_version) = sensor["chip"].readBMP280ID()
sensor["data"]["chip_id"] = chip_id
sensor["data"]["chip_version"] = chip_version

# ============================== Functions ====================================
def get_sensor_values():
    (temperature, pressure) = sensor["chip"].readBMP280All()
    sensor["data"]["temperature"] = { "reading": temperature, "units" : "C" }
    sensor["data"]["pressure"] = { "reading": pressure, "units" : "hPa" }
    return sensor["data"]
    

    

# ============================== API Routes ===================================
app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')
    # create an instance of my pi gpio object class.
    # pi_gpio = PiGpio()
    # switch_state = pi_gpio.read_switch()
    # led1_state = pi_gpio.get_led(1)
    # led2_state = pi_gpio.get_led(2)
    # led3_state = pi_gpio.get_led(3)
    # return render_template('index.html', switch=switch_state,
    #                             led1=led1_state,
    #                             led2=led2_state,
    #                             led3=led3_state)
    
# curl --data 'dutyCycle=50' http://localhost:5000/set_pwm
#
@app.route('/set_pwm', methods=['POST'])
def set_pwm():
    new_dutyCycle = int(request.form['dutyCycle'])
    pi_pwm.change_duty_cycle(pi_pwm_channel, new_dutyCycle)
    return "It worked"
    
# ============================ GET: /leds/<state> =============================
# read the LED status by GET method from curl for example
# curl http://iot8e3c:5000/leds/1
# curl http://iot8e3c:5000/leds/2
# curl http://iot8e3c:5000/leds/3
# -----------------------------------------------------------------------------
@app.route("/leds/<int:led_state>", methods=['GET'])
def leds(led_state):
  return "LED State:" + str(pi_gpio.get_led(led_state)) + "\n"


# =============================== GET: /sw ====================================
# read the switch input by GET method from curl for example
# curl http://iot8e3c:5000/switch
# -----------------------------------------------------------------------------
@app.route("/sw", methods=['GET'])
def sw():
  return "Switch State:" + str(pi_gpio.read_switch()) + "!\n"

# ======================= POST: /ledcmd/<data> =========================
# set the LED state by POST method from curl. For example:
# curl --data 'led=1&state=ON' http://iot8e3c:5000/ledcmd
# -----------------------------------------------------------------------------
@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    cmd_data = request.data
    print "LED Command:" + cmd_data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pi_gpio.set_led(led,False)
    elif (state == 'ON'):
        pi_gpio.set_led(led,True)
    else:
        return "Argument Error"

    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"


# ======================= POST: /motor_multistep/<dir> =========================
# set the motor multistep by POST method from curl. For example:
# curl --data 'steps=10&direction=CW' http://iot8e3c:5000/motor_multistep
# -----------------------------------------------------------------------------
@app.route("/motor_multistep", methods=['POST'])
def postMotorMultistep():
    ctrl_data = request.data
    print "Motor Control Data:" + ctrl_data
    direction = str(request.form['direction'])
    if (direction == 'CW'):
        pi_smc.setDirection(1)
    elif (direction == 'CCW'):      # start
        pi_smc.setDirection(0)
    else:
        return ('Unknown Stepper Motor Direction!', 400)

    steps = str(request.form['steps'])
    pi_smc.step(int(steps))

    return "Motor Multisteps Steps:" + steps + " Direction:"+ direction + "\n"


# ============================= POST: /motor/<state> ============================
# control motor by POST methods from curl for example
# curl http://iot8e3c:5000/motor/0
# curl http://iot8e3c:5000/motor/1
# -----------------------------------------------------------------------------
@app.route("/motor/<int:motor_state>", methods=['GET'])
def motor(motor_state):
    if motor_state == 0:    # stop
        pi_smc.stop()
    elif motor_state == 1:      # start
        pi_smc.start()
    else:
        return ('Unknown Stepper Motor state!', 400)
    return ('', 204)



# ====================== GET: /motor_speed/<speed_rpm> ========================
# set the motor speed in RPM by GET method from curl. For example:
# curl http://iot8e3c:5000/motor_speed/60
# -----------------------------------------------------------------------------
@app.route("/motor_speed/<int:motor_speed>", methods=['GET'])
def set_motor_speed(motor_speed):
    pi_smc.setSpeed(motor_speed)
    return "Set Motor Speed : " + str(pi_smc.getSpeed()) + "\n"


# ===================== GET: /motor_direction/<direction> =====================
# set the motor direction (CW/CCW) by GET method from curl. For example:
# curl http://iot8e3c:5000/motor_direction/1
# -----------------------------------------------------------------------------
@app.route("/motor_zero", methods=['GET'])
def set_motor_zero():
    pi_smc.setPosition(0)
    return "Set Motor Position : " + str(pi_smc.getPosition()) + "\n"


# ===================== GET: /motor_direction/<direction> =====================
# set the motor direction (CW/CCW) by GET method from curl. For example:
# curl http://iot8e3c:5000/motor_direction/1
# -----------------------------------------------------------------------------
@app.route("/motor_direction/<string:direction>", methods=['GET'])
def set_motor_dir(direction):
    pi_smc.setDirection(direction)
    return "Set Motor Direction : " + str(pi_smc.getDirection()) + "\n"


# ===================== GET: /motor_steps/<steps> =====================
# set the motor steps (int) by HTTP GET method  CURL example:
# curl http://iot8e3c:5000/motor_steps/100
# -----------------------------------------------------------------------------
@app.route("/motor_steps/<int:steps>", methods=['GET'])
def set_motor_steps(steps):
    pi_smc.setSteps(steps)
    return "Set Motor Steps : " + str(pi_smc.getSteps()) + "\n"


# ====================== GET: /motor_position/<position> ======================
# set the motor position by HTTP GET method. CURL example:
# curl http://iot8e3c:5000/motor_position/1
# -----------------------------------------------------------------------------
@app.route("/motor_position/<int:position>", methods=['GET'])
def set_motor_pos(position):
    pi_smc.setDirection(direction)
    return "Set Motor Direction : " + str(pi_smc.getDirection()) + "\n"


# -----------------------------------------------------------------------------

# =========================== Endpoint: /myData ===============================
# read the gpio states by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_state_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            
            led_red = str(pi_gpio.get_led(1))
            led_yel = str(pi_gpio.get_led(2))
            led_grn = str(pi_gpio.get_led(3))

            return_value = get_sensor_values()
            return_value["led_red"] = led_red
            return_value["led_grn"] = led_yel
            return_value["led_yel"] = led_grn 
            
            #return_value["motor"]["position"] = str(pi_smc.getPosition())
            #return_value["motor"]["state"] = str(pi_smc.getState())

            yield('data: {0}\n\n'.format(json.dumps(return_value)))
            time.sleep(1.0)
    return Response(get_state_values(), mimetype='text/event-stream')
# ============================== API Routes ===================================

if __name__ == "__main__":
    pi_pwm_channel = pi_pwm.start_pwm(1, 100, 0)
    app.run(host='0.0.0.0', debug=True, threaded=True)
